package brass;

import java.util.Comparator;

public class BrassCompareAmountSpent implements Comparator<BrassPlayer>
{
	private boolean asc;

    public BrassCompareAmountSpent(boolean x)
    {
        asc = x;
    }

    public int compare(BrassPlayer p1, BrassPlayer p2) {
    	int spend1 = p1.getAmountSpent();
    	int spend2 = p2.getAmountSpent();

    	if(spend1 == spend2)
    		return 0;
    	else {
    		if(asc)
    			return spend1 < spend2 ? -1 : 1;
    		else
    			return spend1 < spend2 ? 1 : -1;
    	}
    }
}