package pqsort;

import java.util.Comparator;

public class PQFactory {
	/**
	* @param comp comparator for elements
	* @param pq_type type (enum) of the data stucture to use
	* @return Resturns PQInterface that is set to BST / SLL depending on given param
	*/
	public static <E> PQInterface<E> createPQ(Comparator<E> comp, PQType pq_type) {
		PQInterface<E> retVal;

		if(pq_type == PQType.TREE) {
			retVal = new PQBST<E>(comp); 
		} else {
			retVal = new PQSLL<E>(comp);
		}

		return retVal;
	}
}