package pqsort;

public interface PQInterface <E> {
	/**
	* @param item Item to insert
	*/
	public void pqInsert(E item);

	/**
	* @return Return back removed item
	* @throws PQException in case if pq has no items
	*/
	public E pqRemove() throws PQException;

	/**
	* @return Returns true if pq is empty
	*/
	public boolean pqIsEmpty();
}