package pqsort;

import java.util.Comparator;

class PQSLL<E> implements PQInterface<E>
{
	private SortedListLinked<E> sll;
	
	public PQSLL(Comparator<E> comp)
	{
		sll = new SortedListLinked<E>(comp);
	}
	
	public void pqInsert(E item)
	{
		sll.add(item);
	}
	
	public E pqRemove() throws PQException
	{
		//problem if item is null
		E item = sll.remove();

		if(item == null)
			throw new PQException("SLL is empty, noting to remove");

		return item;
	}
	
	public boolean pqIsEmpty()
	{
		return (sll.size() == 0);
	}
	
}
