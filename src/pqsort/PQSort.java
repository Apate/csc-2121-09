package pqsort;

import java.util.Comparator;
import java.util.ArrayList;
import java.util.List;

public class PQSort {
	/**
	* @param unsorted_items a list of unsorted items
	* @param comp comparator for elements
	* @param pq_type type (enum) of the data stucture to use
	* @return resturns a list with sorted items
	*/
	public static <E> List<E> pqSort(List<E> unsorted_items, Comparator<E> comp, PQType pq_type) {
		PQInterface<E> sorter = PQFactory.createPQ(comp, pq_type);
		List<E> sorted = new ArrayList();

		for(E itm : unsorted_items) {
			sorter.pqInsert(itm);
		}

		while(!sorter.pqIsEmpty()) {
			sorted.add(sorter.pqRemove());
		}

		return sorted;
	}
}