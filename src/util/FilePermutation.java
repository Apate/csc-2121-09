package util;

import java.util.List;
import java.util.ArrayList;

public class FilePermutation implements Permutation
{
   private int r;
   private List<Integer> numbers;
   private ReadTextFile file;

/**
 *@param file_name variable for the text file name that contains the numbers for permutation
 *@param r the number variable for permutation  
 *@throws none
 *@returns constructor
 *This is the constructor for FilePermutation class
 *
*/
   
   
   //pick r of them from 1 to n
   public FilePermutation(String file_name, int r)
   {
	   numbers = new ArrayList<Integer>();
		file = new ReadTextFile(file_name);
		
		String text = file.readLine();
		
		while (!file.EOF())
		{
			numbers.add(Integer.parseInt(text));
			text = file.readLine();
		}
		
		file.close();
		this.r = r;
   }
   
   /**
    *@param none 
    *@return returns true/false depending if there is another number in the list
    *@throws none 
    */
   
   
   public boolean hasNext()
   {
		return (r > 0);
   }

   /*
    *@param none
    *@return returns the value of the next number in sequence 
    *@throws none
    */
   public int next()
   {
		if (r == 0) return -1;

		int value = numbers.get(0);
		numbers.remove(0);
		r--;
		return value;
   }
}
