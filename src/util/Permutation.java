package util;

/**
 *@param none 
 *@return none 
 *@throws none
 *
 *This interface for creates a Permutation, checks for next. 
 *
*/


public interface Permutation
{
   public boolean hasNext();
   public int next();
}
