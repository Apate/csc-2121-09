package util;

/**
 *@param none 
 *@return none 
 *@throws none
 *
 *This interface for creates a Permutation, checks for next. 
 *
*/


public class PermutationFactory

/**
 *@param file_name  this string is the fire name
 *@param r this is the first number used in a permutation
 *@param n this is the second number used in a permutation 
 *@return none 
 *@throws none
 *
 *This interface for creates a Permutation, checks for next. 
 *
*/
{
   public static Permutation getPermutation(String file_name, int r, int n)
   {
	   return new FilePermutation(file_name, r);
	   //return new RandomPermutation(r, n);
   }
}
